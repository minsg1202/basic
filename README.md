logAdvice
```java
String param = getParamStr(joinPoint));

private String getParamStr(JoinPoint joinPoint) {
    StringBuffer sb = new StringBuffer();
    CodeSignature cs = (CodeSignature) joinPoint.getSignature();
    String[] parameterNames = cs.getParameterNames();
    Object[] args = joinPoint.getArgs();

    Map<String, Object> params = new HashMap<>();
    for (int i=0; i < parameterNames.length; i++) {
        if(!"".equals(sb.toString())) {
            sb.append(",");
        }
        sb.append(parameterNames[i]);
        sb.append("=");
        sb.append(args[i]);
    }

    return sb.toString();
}
```

JwtUtil

```java

Map<String, Object> tokenMap = new HashMap<String, Object>();
tokenMap.put("sno", sno);
tokenMap.put("type", "acess");
tokenMap.put("profile", "local");
tokenMap.put("platform", "b2c");

...
.claim("data", AES256Util.encrypt(BXMUtil.toJson(tokenMap)))
.....

```
