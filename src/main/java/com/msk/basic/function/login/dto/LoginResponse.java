package com.msk.basic.function.login.dto;

import com.msk.basic.common.model.BaseTokenResponse;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "로그인 응답 모델")
public class LoginResponse extends BaseTokenResponse {

    @ApiModelProperty(value = "고객명", example = "김고객", position = 1)
    private String custNm;

}
