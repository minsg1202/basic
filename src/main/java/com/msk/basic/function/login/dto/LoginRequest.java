package com.msk.basic.function.login.dto;

import com.msk.basic.common.model.BaseRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "로그인 요청 모델")
public class LoginRequest extends BaseRequest {

    @NotBlank(message = "로그인 ID는 필수값입니다.")
    @ApiParam(value = "로그인 ID", required = true, example = "loginId1234")
    private String id;

    @NotBlank(message = "비밀번호는 필수값입니다.")
    @ApiParam(value = "비밀번호", required = true, example = "password1234")
    private String password;

}
