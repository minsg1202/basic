package com.msk.basic.function.login.service;

import com.msk.basic.common.enumeration.ErrorCode;
import com.msk.basic.common.exception.CustomException;
import com.msk.basic.common.model.CustVO;
import com.msk.basic.common.model.ResponseModel;
import com.msk.basic.common.util.JwtUtil;
import com.msk.basic.common.util.RestUtil;
import com.msk.basic.function.login.dto.LoginRequest;
import com.msk.basic.function.login.dto.LoginResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

@Service
@AllArgsConstructor
public class LoginService {

    private JwtUtil jwtUtil;

    public ResponseEntity<ResponseModel<LoginResponse>> login(LoginRequest input, Errors errors) {
        if ("loginId1234".equals(input.getId()) && "password1234".equals(input.getPassword())) {
            CustVO custVO = CustVO.builder().custSno(1).custNm("유저").build();
            return RestUtil.ok(RestUtil.convert(jwtUtil.createLoginToken(custVO), LoginResponse.class));
        } else {
            throw new CustomException(ErrorCode.ERROR);
        }
    }
}
