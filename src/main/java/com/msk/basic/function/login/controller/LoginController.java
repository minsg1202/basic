package com.msk.basic.function.login.controller;

import com.msk.basic.common.config.TagsConfig;
import com.msk.basic.common.model.ResponseModel;
import com.msk.basic.function.login.dto.LoginRequest;
import com.msk.basic.function.login.dto.LoginResponse;
import com.msk.basic.function.login.service.LoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

@AllArgsConstructor
@RestController
@RequestMapping("/api")
@Api(tags = TagsConfig.TAGS_LOG)
public class LoginController {

    private LoginService loginService;

    @GetMapping("/login")
    @ApiOperation(value = "간편 로그인")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Access Token(불필요)", required = false, dataType = "String", paramType = "header", defaultValue = "")
    })
    public ResponseEntity<ResponseModel<LoginResponse>> login(@Valid LoginRequest input, @ApiIgnore Errors errors) throws BindException {
        return loginService.login(input, errors);
    }

}
