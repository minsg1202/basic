package com.msk.basic.function.customer.controller;

import com.msk.basic.common.config.TagsConfig;
import com.msk.basic.common.model.ResponseModel;
import com.msk.basic.function.customer.dto.request.CustomerRequest;
import com.msk.basic.function.customer.dto.response.CustomerResponse;
import com.msk.basic.function.customer.service.CustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

@AllArgsConstructor
@RestController
@RequestMapping("/api")
@Api(tags = TagsConfig.TAGS_SIN)
public class CustomerController {

    private CustomerService customerService;

    @PostMapping("/sign-in")
    @ApiOperation(value = "회원가입")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Access Token(불필요)", required = false, dataType = "String", paramType = "header", defaultValue = "")
    })
    public ResponseEntity<ResponseModel<CustomerResponse>> signIn(@Valid @RequestBody CustomerRequest input, @ApiIgnore Errors errors) throws BindException {
        return customerService.signIn(input, errors);
    }

}
