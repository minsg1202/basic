package com.msk.basic.function.customer.service;

import com.msk.basic.common.model.CustVO;
import com.msk.basic.common.model.ResponseModel;
import com.msk.basic.common.util.JwtUtil;
import com.msk.basic.common.util.RestUtil;
import com.msk.basic.function.customer.dto.request.CustomerRequest;
import com.msk.basic.function.customer.dto.response.CustomerResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

@Service
@AllArgsConstructor
public class CustomerService {

    private JwtUtil jwtUtil;

    public ResponseEntity<ResponseModel<CustomerResponse>> signIn(CustomerRequest input, Errors errors) {
        CustVO custVO = RestUtil.convert(input, CustVO.class);
        return RestUtil.ok(RestUtil.convert(jwtUtil.createLoginToken(custVO), CustomerResponse.class));
    }

}
