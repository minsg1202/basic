package com.msk.basic.function.customer.dto.request;

import com.msk.basic.common.model.BaseRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "회원가입 요청 모델")
public class CustomerRequest extends BaseRequest {

    @NotBlank(message = "로그인 ID는 필수값입니다.")
    @ApiModelProperty(value = "로그인 ID", required = true, example = "loginId1234", position = 1)
    private String id;

    @NotBlank(message = "비밀번호는 필수값입니다.")
    @ApiModelProperty(value = "비밀번호", required = true, example = "password1234", position = 2)
    private String password;

    @NotBlank(message = "고객명은 필수값입니다.")
    @ApiModelProperty(value = "고객명", required = true, example = "유저", position = 3)
    private String custNm;

}
