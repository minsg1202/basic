package com.msk.basic.common.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorCode {

    //SERVER ERROR
    ERROR("ERR001", "장애가 발생하였습니다."),
    TOKEN_EXPIRED("ERR104", "토큰이 만료 되었습니다."),
    TOKEN_FALSIFY("ERR105", "토큰이 위변조 되었습니다."),

    // CUSTOM ERROR
    INVALID_PARAM("ERR301", "유효성 검사에서 에러가 발생하였습니다.")
    ;

    /**
     * 에러 코드
     */
    private final String code;
    /**
     * 에러 메세지
     */
    private final String message;

}
