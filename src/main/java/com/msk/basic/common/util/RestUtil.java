package com.msk.basic.common.util;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.msk.basic.common.enumeration.ErrorCode;
import com.msk.basic.common.model.ResponseModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@RequiredArgsConstructor
public class RestUtil {

    private static final ObjectMapper objectMapper; // ObjectMapper

    private static final String SUC_CODE = "SUC001";
    private static final String SUC_MSG = "처리가 완료되었습니다.";

    static { // ObjectMapper 생성 및 설정
        objectMapper = new ObjectMapper()
                /*
                 * convertValue 를 사용할 때
                 * fromValue 객체에는 있지만 toValueType 객체에는 없는 필드가 있으면 에러가 발생합니다.
                 * 해당 에러를 발생하지 않기 위해서
                 * fromValue 객체에는 있지만 toValueType 객체에는 없는 필드를 무시하도록 설정
                 */
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static ResponseEntity<?> ok() {
        return ok(null);
    }

    public static <T> ResponseEntity<ResponseModel<T>> ok(final T body) {
        return ok(body, SUC_CODE, SUC_MSG);
    }

    public static <T> ResponseEntity<ResponseModel<T>> ok(final T body, final String code, final String message) {
        log.info("RestUtil.ok");

        ResponseModel<T> model = new ResponseModel<T>();

        model.setCode(code);
        model.setMessage(message);
        model.setData(body);

        return ResponseEntity
                .ok()
                .body(model);
    }

    public static ResponseEntity<?> error() {
        log.info("RestUtil.error");

        return error(ErrorCode.ERROR, null);
    }

    public static ResponseEntity<?> error(final String errorMsg) {
        log.info("RestUtil.error(errorMsg)");

        return error(ErrorCode.ERROR, errorMsg);
    }

    public static ResponseEntity<?> error(final ErrorCode errorCode) {
        log.info("RestUtil.error(errorCode)");

        return error(errorCode, errorCode.getMessage());
    }

//    public static ResponseEntity<?> error(final ErrorField errorField) {
//        log.info("RestUtil.error(errorField)");
//
//        return error(BAD_REQUEST, ErrorCode.INVALID_PARAM, errorField.getErrors().toString(), errorField);
//    }

    public static ResponseEntity<?> error(final ErrorCode errorCode, final String errorMsg) {
        log.info("RestUtil.error(errorCode,errorMsg)");

        return error(BAD_REQUEST, errorCode, errorMsg);
    }

    public static ResponseEntity<?> tokenError(ErrorCode errorCode) {
        log.info("RestUtil.tokenError");

        return error(UNAUTHORIZED, errorCode, null);
    }

    public static ResponseEntity<?> serverError(final String errorMsg) {
        log.info("RestUtil.serverError");

        return error(INTERNAL_SERVER_ERROR, ErrorCode.ERROR, errorMsg);
    }

    public static ResponseEntity<?> error(final HttpStatus status, final ErrorCode errorCode, final String errorMsg ) {
        log.info("RestUtil.error");

        return error(status, errorCode, errorMsg, null);
    }

    public static <T> ResponseEntity<?> error(final HttpStatus status, final ErrorCode errorCode, final String errorMsg, final T errorField ) {
        log.info("RestUtil.error");

        return error(status, errorCode.getCode(), errorCode.getMessage(), errorMsg, errorField );
    }

    public static <T> ResponseEntity<?> error(final HttpStatus status, final String code, final String message, final String errorMsg, final T errorField ) {
        log.info("RestUtil.error");

        return ResponseEntity
                .status(status)
                .body(objectMapper.valueToTree(
                        ResponseModel
                                .builder()
                                .code(code)
                                .message(message)
                                .data(errorField)
                                .build()
                ));
    }

    public static <B, A> A convert(final B before, final Class<A> after) {
        return objectMapper.convertValue(before, after);
    }

    public static <B, A> List<A> convert(final List<B> before, final Class<A> after) {
        return before.stream().map(item -> convert(item, after)).collect(Collectors.toList());
    }

    public static Map<String, Object> toMap (String jsonString) {
        try {
            return objectMapper.readValue(jsonString, Map.class);
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

    public static String getClientIp (final HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");

        if (null == ip) {
            ip = request.getRemoteAddr();
        }

        return ip;
    }
}
