package com.msk.basic.common.util;

import com.msk.basic.common.model.GlobalVariable;
import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Component;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Component
public class EncryptionUtil {

    public String encryptSHA512(String data) {
        String encText = null;

        try {
            MessageDigest sha = MessageDigest.getInstance("SHA-512");
            sha.update(data.getBytes());
            StringBuffer sb = new StringBuffer();
            for (byte b : sha.digest()) {
                sb.append(Integer.toHexString(0xff & b));
            }
            encText = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

        return encText;
    }

    public String encryptAES256(String plainText) {
        String key = GlobalVariable.GLOBAL_ENCRYPTION_KEY;
        String enStr = null;

        try {
            byte[] keyData = key.length() > 32 ? key.substring(0, 32).getBytes() : key.getBytes();
            byte[] newKeyData = new byte[32];
            for (int i = 0; i < keyData.length; i++) {
                newKeyData[i] = keyData[i];
            }

            String IV = key.substring(0, 16);
            byte[] newKeyData2 = new byte[16];
            byte[] ivByte = IV.getBytes();
            for (int i = 0; i < ivByte.length; i++) {
                newKeyData2[i] = IV.getBytes()[i];
            }

            SecretKey secureKey = new SecretKeySpec(newKeyData, "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secureKey, new IvParameterSpec(newKeyData2));

            byte[] encrypted = cipher.doFinal(plainText.getBytes(StandardCharsets.UTF_8));
            enStr = new String(Base64.encodeBase64(encrypted));

        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        } catch (NoSuchPaddingException ex) {
            ex.printStackTrace();
        } catch (InvalidKeyException ex) {
            ex.printStackTrace();
        } catch (InvalidAlgorithmParameterException ex) {
            ex.printStackTrace();
        } catch (IllegalBlockSizeException ex) {
            ex.printStackTrace();
        } catch (BadPaddingException ex) {
            ex.printStackTrace();
        }

        return enStr;
    }

    public String decryptAES256(String plainText) {
        String key = GlobalVariable.GLOBAL_ENCRYPTION_KEY;
        String deStr = null;

        try {
            byte[] keyData = key.length() > 32 ? key.substring(0, 32).getBytes() : key.getBytes();
            byte[] newKeyData = new byte[32];
            for (int i = 0; i < keyData.length; i++) {
                newKeyData[i] = keyData[i];
            }

            String IV = key.substring(0, 16);
            byte[] newKeyData2 = new byte[16];
            byte[] ivByte = IV.getBytes();
            for (int i = 0; i < ivByte.length; i++) {
                newKeyData2[i] = IV.getBytes()[i];
            }

            SecretKey secureKey = new SecretKeySpec(newKeyData, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, secureKey, new IvParameterSpec(newKeyData2));

            byte[] byteStr = Base64.decodeBase64(plainText.getBytes());

            deStr = new String(cipher.doFinal(byteStr), StandardCharsets.UTF_8);

        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        } catch (NoSuchPaddingException ex) {
            ex.printStackTrace();
        } catch (InvalidKeyException ex) {
            ex.printStackTrace();
        } catch (InvalidAlgorithmParameterException ex) {
            ex.printStackTrace();
        } catch (IllegalBlockSizeException ex) {
            ex.printStackTrace();
        } catch (BadPaddingException ex) {
            ex.printStackTrace();
        }

        return deStr;
    }

}
