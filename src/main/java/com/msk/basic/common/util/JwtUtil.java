package com.msk.basic.common.util;

import com.msk.basic.common.config.SwaggerConfig;
import com.msk.basic.common.model.CustVO;
import com.msk.basic.common.model.GlobalVariable;
import com.msk.basic.common.model.TokenVO;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

@Slf4j
@Component
public class JwtUtil {

    private Map<String, Object> headerMap = new HashMap<String, Object>();
    private EncryptionUtil encryptionUtil = new EncryptionUtil();

    public JwtUtil() {
        // 해더 설정
        headerMap.put("typ", "JWT");
        headerMap.put("alg", "HS256");
    }

    private String tokenType = "this application name";

    public TokenVO createLoginToken(CustVO custVO) {
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(GlobalVariable.GLOBAL_JWT_SECRET_KEY);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        Date refreshExDate = null;
        Date accessExDate = null;
        Calendar refreshCal = Calendar.getInstance();
        Calendar accessCal = Calendar.getInstance();

        refreshCal.add(Calendar.MINUTE, Integer.parseInt(GlobalVariable.GLOBAL_REFRESH_TOKEN_EXPIRED_MINUTE));
        refreshExDate = new Date(refreshCal.getTimeInMillis());

        accessCal.add(Calendar.MINUTE, Integer.parseInt(GlobalVariable.GLOBAL_ACCESS_TOKEN_EXPIRED_MINUTE));
        accessExDate = new Date(accessCal.getTimeInMillis());

        String encCustSno = encryptionUtil.encryptAES256(String.valueOf(custVO.getCustSno()));
        String encCustNm = encryptionUtil.encryptAES256(custVO.getCustNm());
        String encIsRefreshToken = encryptionUtil.encryptAES256("Y");
        String encIsAccessToken = encryptionUtil.encryptAES256("Y");
        String encTokenType = encryptionUtil.encryptAES256(tokenType);

        // 리프레쉬 토큰 생성 [S]
        JwtBuilder builder = Jwts.builder().setHeader(headerMap)
                .claim("custSno", encCustSno)
                .claim("custNm", encCustNm)
                .claim("isRefreshToken", encIsRefreshToken)
                .claim("tokenType", encTokenType)
                .setExpiration(refreshExDate)
                .signWith(signatureAlgorithm, signingKey);
        // 리프레쉬 토큰 생성 [E]

        // 엑세스 토큰 생성 [S]
        JwtBuilder builderAccess = Jwts.builder().setHeader(headerMap)
                .claim("custSno", encCustSno)
                .claim("custNm", encCustNm)
                .claim("isAccessToken", encIsAccessToken)
                .claim("tokenType", encTokenType)
                .setExpiration(accessExDate)
                .signWith(signatureAlgorithm, signingKey);
        // 액세스 토큰 생성 [E]

        String refreshToken = builder.compact();
        String accessToken = builderAccess.compact();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.getDefault());
        String refreshExprDtm = sdf.format(refreshExDate);
        String accessExprDtm = sdf.format(accessExDate);

        log.info("[INFO] [TOKEN] 발급된 토큰정보");
        log.info("[INFO] [TOKEN] 고객 일련번호 \t: {}", custVO.getCustSno());
        log.info("[INFO] ACCESS TOKEN \t: {}", accessToken);
        log.info("[INFO] ACCESS TOKEN 만료시간 \t: {}", accessExprDtm);
        log.info("[INFO] REFRESH TOKEN \t: {}", refreshToken);
        log.info("[INFO] REFRESH TOKEN 만료시간 \t: {}", refreshExprDtm);

        TokenVO tokenVO = TokenVO.builder()
                            .custNm(custVO.getCustNm())
                            .accessToken(accessToken)
                            .accessTokenExpiredDate(accessExprDtm)
                            .refreshToken(refreshToken)
                            .refreshTokenExpiredDate(refreshExprDtm)
                            .build();

        return tokenVO;
    }

    public TokenVO createAccessToken(String token) {
        try {
            SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

            Claims claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(GlobalVariable.GLOBAL_JWT_SECRET_KEY))
                    .parseClaimsJws(token).getBody(); // 정상 수행된다면 해당 토큰은 정상토큰

            String decCustSno = getClaimsDataString(claims, "custSno");

            log.info("[INFO] [TOKEN] 요청된 토큰정보");
            log.info("[INFO] [TOKEN] 고객 일련번호 \t: {}", decCustSno);

            Date exDate = null;
            Calendar cal = Calendar.getInstance();

            cal.add(Calendar.MINUTE, Integer.parseInt(GlobalVariable.GLOBAL_ACCESS_TOKEN_EXPIRED_MINUTE));
            exDate = new Date(cal.getTimeInMillis());

            byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(GlobalVariable.GLOBAL_JWT_SECRET_KEY);
            Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

            // 엑세스 토큰 생성 [S]
            JwtBuilder builderAccess = Jwts.builder().setHeader(headerMap)
                    .claim("custSno", decCustSno)
                    .claim("custNm", getClaimsDataString(claims, "custNm"))
                    .claim("isAccessToken", encryptionUtil.encryptAES256("Y"))
                    .claim("tokenType", encryptionUtil.encryptAES256(tokenType))
                    .setExpiration(exDate)
                    .signWith(signatureAlgorithm, signingKey);
            // 액세스 토큰 생성 [E]

            String accessToken = builderAccess.compact();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.getDefault());
            String exprDtm = sdf.format(exDate);

            log.info("[INFO] [TOKEN] 발급된 토큰정보");
            log.info("[INFO] [TOKEN] 고객 일련번호 \t: {}", decCustSno);
            log.info("[INFO] [TOKEN] 만료시간 \t: {}", exprDtm);

            TokenVO tokenVO = TokenVO.builder()
                    .accessToken(accessToken)
                    .accessTokenExpiredDate(exprDtm)
                    .build();

            return tokenVO;
        } catch (ExpiredJwtException exception) {
            // Token_Expired Exception
            exception.printStackTrace();
        } catch (JwtException exception) {
            // TOKEN_FALSIFY Exception
            exception.printStackTrace();
        }
        return null;
    }

    public TokenVO updateAccessToken(String token) {
        try {
            Claims claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(GlobalVariable.GLOBAL_JWT_SECRET_KEY))
                    .parseClaimsJws(token).getBody(); // 정상 수행된다면 해당 토큰은 정상토큰

            String decCustSno = getClaimsDataString(claims, "custSno");

            log.info("========================== updateAccessToken ======================");
            log.info("[INFO] [TOKEN] 요청된 토큰정보");
            log.info("[INFO] [TOKEN] 고객 일련번호 \t: {}", decCustSno);

            // refreshToken check
            if (!isRefreshToken(claims)) {
                // TOKEN_FALSIFY Exception
            }

            Calendar todayCal = Calendar.getInstance();

            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(claims.getExpiration().getTime());
//            cal.add(Calendar.MINUTE, Integer.parseInt(refreshTokenChangeMinute));

            CustVO custVO = CustVO.builder()
                            .custSno(Integer.parseInt(decCustSno))
                            .custNm(getClaimsDataString(claims, "custNm"))
                            .build();

            if (todayCal.getTimeInMillis() > cal.getTimeInMillis()) {
                return createLoginToken(custVO);
            } else {
                return createAccessToken(token);
            }
        } catch (Exception exception) {
            // TOKEN_FALSIFY Exception
            exception.printStackTrace();
        }
        return null;
    }

    public Long isUseableToken(String token) {
        Long result = null;
        try {
            Claims claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(GlobalVariable.GLOBAL_JWT_SECRET_KEY))
                    .parseClaimsJws(token).getBody(); // 정상 수행된다면 해당 토큰은 정상토큰

            if (!isTokenType(claims)) {
                // TOKEN_FALSIFY Exception
            }

            Long custSno = getClaimsDataLong(claims, "custSno");

            // 공통 체크
            commonCheck();

            if ("Y".equals(claims.get("isRefreshToken"))) {
                log.info("[INFO] [ACTIONLOG] [TOKEN] 타입 \t: REFRESH TOKEN");
                result = custSno;
            } else if ("Y".equals(claims.get("isAccessToken"))) {
                log.info("[INFO] [ACTIONLOG] [TOKEN] 타입 \t: ACCESS TOKEN");
                result = custSno;
            } else {
                // Token_Expired Exception
            }

        } catch (ExpiredJwtException exception) {
            // Token_Expired Exception
            exception.printStackTrace();
        } catch (JwtException exception) {
            // TOKEN_FALSIFY Exception
            exception.printStackTrace();
        }
        return result;
    }

    private void commonCheck() {

    }

    private String getClaimsDataString(final Claims claims, final String key) {
        return encryptionUtil.decryptAES256((String) claims.get(key));
    }

    private Long getClaimsDataLong(final Claims claims, final String key) {
        return Long.parseLong(encryptionUtil.decryptAES256((String) claims.get(key)));
    }

    private boolean isTokenType(Claims claims) {
        return tokenType.equals(encryptionUtil.decryptAES256((String) claims.get("tokenType")));
    }

    private boolean isRefreshToken(Claims claims) {
        return "Y".equals(encryptionUtil.decryptAES256((String) claims.get("isRefreshToken"))) || isTokenType(claims);
    }

    public String getLoginApchTknVal(HttpServletRequest request) {
        String token = request.getHeader(SwaggerConfig.HEADER_AUTH);
        if (Pattern.matches("^Bearer .*", token)) {
            token = token.replaceAll("^Bearer( )*", "");
        }
        return token;
    }

}
