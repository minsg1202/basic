package com.msk.basic.common.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.msk.basic.common.exception.CustomException;
import com.msk.basic.common.model.GlobalVariable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class LogUtil {

    public static void setActionLog(HttpServletRequest request, Throwable ex, String param, String result) {
        if (null == request) {
            request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        }

        if (null == ex) {
            // 성공
            log.debug("[INFO] [ACTIONLOG] [SUCCESS] [{} {}], PARAM_DATA : {}, RESULT_DATA : {}", request.getMethod(), request.getRequestURI(), param, result);
        } else {
            // 에러
            ObjectMapper objectMapper = new ObjectMapper();;
            String errorMessage = "";
            String exceptionType = "";
            String cause = "";

            // 상세 exception 정의
            if (ex instanceof Exception) {
                String exceptionClass = ex.getClass().toString();
                exceptionType = exceptionClass.substring(exceptionClass.lastIndexOf(".") + 1, exceptionClass.length());
            }

            String method = request.getMethod();
            String requestUri = request.getRequestURI();
            errorMessage = StringUtils.isEmpty(errorMessage) ? ex.getMessage() : errorMessage;

            List<String> stackTraceList = new ArrayList<String>();

            for (StackTraceElement element : ex.getStackTrace()) {
                String target = element.toString();
                if (target.contains(GlobalVariable.GLOBAL_BASE_PACKAGE)) {
                    stackTraceList.add("[ERROR] at " + element);
                }
            }

            String stackTrace = String.join(",", stackTraceList);

            log.error("===============================================================================================");
            log.error("[INFO] [ACTIONLOG] [ERROR] [{} {}], PARAM_DATA : {}, ErrorLog = {}", method, requestUri, param);
            log.error("[INFO] [ACTIONLOG] [ERROR] Request URI \t: " + request.getRequestURI());
            log.error("[INFO] [ACTIONLOG] [ERROR] Request Method \t: " + request.getMethod());
            printError(ex);
            log.error("===============================================================================================");
        }
    }

    private static void printError(Throwable error) {
        if (!ObjectUtils.isEmpty(error)) {
            if (!ObjectUtils.isEmpty(error.getCause())) {
                log.error("[INFO] [ACTIONLOG] [ERROR] Cause \t\t: " + error.getCause());
            }
            if (!org.springframework.util.StringUtils.isEmpty(error.getMessage())) {
                log.error("[INFO] [ACTIONLOG] [ERROR] Message \t\t: " + error.getMessage());
            }

            if (error instanceof CustomException) {
                CustomException exception = (CustomException) error;
                if (!org.springframework.util.StringUtils.isEmpty(exception.getErrorCode()) && !org.springframework.util.StringUtils.isEmpty(exception.getErrorCode().getMessage())) {
                    log.error("[INFO] [ACTIONLOG] [ERROR] Message \t\t: " + exception.getErrorCode().getMessage());
                }
            }

            for (StackTraceElement element : error.getStackTrace()) {
                String target = element.toString();
                if (target.contains(GlobalVariable.GLOBAL_BASE_PACKAGE)) {
                    log.error("[INFO] [ACTIONLOG] [ERROR] at " + element);
                }
            }
        }
    }

}
