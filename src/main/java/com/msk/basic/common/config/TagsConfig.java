package com.msk.basic.common.config;

import org.springframework.stereotype.Component;

@Component
public class TagsConfig {

    public final static String TAGS_COM = "00_공통";
    public final static String TAGS_LOG = "01_로그인";
    public final static String TAGS_SIN = "02_회원가입";

}
