package com.msk.basic.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    public static final String HEADER_AUTH = "Authorization";

    public static final String MASTER_TOKEN = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjdXN0U25vIjo3MywiY3VzdE5tIjoi7JWI7ZiE7ISgIiwiZXhBY2Nlc3NDcmVhdGVEYXRlIjoxNjQ0OTc2OTIxMDU2LCJpc0FjY2Vzc1Rva2VuIjoiWSIsInRva2VuVHlwZSI6ImIyYyIsImV4cCI6MTY0NzU2ODkyMX0.8G1Fin8akIaghq_fRdc9FxipIvvMZEU8TPdvCM9xEAQ";
    public static final String MASTER_TOKEN_Expired_Date = "2022.03.18 11:02:01";

    public static final String APP_DESCRIPTION = "## 토큰 ##\n"
                                               + "- 토큰 타입 `Bearer`는 필수입니다.\n\n"
                                               + "## HTTP status code 정의 ## \n"
                                               + "status|설명 \n"
                                               + "-|- \n"
                                               + "200 | 성공 \n"
                                               + "400 | 클라이언트 에러 (코드 확인) \n"
                                               + "401 | 인증 에러 \n"
                                               + "403 | 인가 에러 \n"
                                               + "500 | 서버 에러 -> 백엔드팀에 문의 \n"
                                               ;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.ant("/api/**"))
                .build()
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false)
                .globalOperationParameters(Arrays.asList(
                        new ParameterBuilder().name(HEADER_AUTH).description("Access Token(만료일자 : "+ MASTER_TOKEN_Expired_Date + ")").defaultValue(MASTER_TOKEN)
                                .modelRef(new ModelRef("string")).parameterType("header").required(true).build()))
                ;
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("이 문서는 Rest Api Project API 문서 입니다.")
                .description(APP_DESCRIPTION)
                .license("Apache License Version 2.0")
                .licenseUrl("https://github.com/IBM-Bluemix/news-aggregator/blob/master/LICENSE").version("1.0").build();
    }
}
