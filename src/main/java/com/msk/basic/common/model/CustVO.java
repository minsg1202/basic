package com.msk.basic.common.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class CustVO {

    private int custSno;
    private String custNm;
    private String loginId;
    private String password;

}
