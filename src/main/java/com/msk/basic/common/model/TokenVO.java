package com.msk.basic.common.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class TokenVO {

    private int custSno;
    private String custNm;
    private String accessToken;
    private String accessTokenExpiredDate;
    private String refreshToken;
    private String refreshTokenExpiredDate;

}
