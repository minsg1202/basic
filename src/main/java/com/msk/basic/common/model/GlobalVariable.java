package com.msk.basic.common.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GlobalVariable {

    public static String GLOBAL_ENCRYPTION_KEY;

    public static String GLOBAL_JWT_SECRET_KEY;

    public static String GLOBAL_REFRESH_TOKEN_EXPIRED_MINUTE;

    public static String GLOBAL_REFRESH_TOKEN_CHANGE_MINUTE;

    public static String GLOBAL_ACCESS_TOKEN_EXPIRED_MINUTE;

    public static String GLOBAL_BASE_PACKAGE;

    @Value("${basic.encryption.key:}")
    private void setGLOBAL_ENCRYPTION_KEY(String GLOBAL_ENCRYPTION_KEY) {
        this.GLOBAL_ENCRYPTION_KEY = GLOBAL_ENCRYPTION_KEY;
    }

    @Value("${basic.jwt.secretKey:}")
    private void setGLOBAL_JWT_SECRET_KEY(String GLOBAL_JWT_SECRET_KEY) {
        this.GLOBAL_JWT_SECRET_KEY = GLOBAL_JWT_SECRET_KEY;
    }

    @Value("${basic.token.expired.refresh:}")
    private void setGLOBAL_REFRESH_TOKEN_EXPIRED_MINUTE(String GLOBAL_REFRESH_TOKEN_EXPIRED_MINUTE) {
        this.GLOBAL_REFRESH_TOKEN_EXPIRED_MINUTE = GLOBAL_REFRESH_TOKEN_EXPIRED_MINUTE;
    }

    @Value("${basic.token.expired.change.refresh:}")
    private void setGLOBAL_REFRESH_TOKEN_CHANGE_MINUTE(String GLOBAL_REFRESH_TOKEN_CHANGE_MINUTE) {
        this.GLOBAL_REFRESH_TOKEN_CHANGE_MINUTE = GLOBAL_REFRESH_TOKEN_CHANGE_MINUTE;
    }

    @Value("${basic.token.expired.access:}")
    private void setGLOBAL_ACCESS_TOKEN_EXPIRED_MINUTE(String GLOBAL_ACCESS_TOKEN_EXPIRED_MINUTE) {
        this.GLOBAL_ACCESS_TOKEN_EXPIRED_MINUTE = GLOBAL_ACCESS_TOKEN_EXPIRED_MINUTE;
    }

    @Value("${basic.base.package:}")
    private void setGLOBAL_BASE_PACKAGE(String GLOBAL_BASE_PACKAGE) {
        this.GLOBAL_BASE_PACKAGE = GLOBAL_BASE_PACKAGE;
    }
}
