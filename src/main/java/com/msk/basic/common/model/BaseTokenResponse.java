package com.msk.basic.common.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "토큰 기본 응답 모델")
public class BaseTokenResponse {

    @ApiModelProperty(value = "엑세스토큰", position = 100)
    private String accessToken;

    @ApiModelProperty(value = "엑세스토큰 만료시간", example = "2022.03.26 16:54:39", position = 101)
    private String accessTokenExpiredDate;

    @ApiModelProperty(value = "리프레시토큰", position = 102)
    private String refreshToken;

    @ApiModelProperty(value = "리프레시토큰 만료시간", example = "2022.03.26 16:54:39", position = 103)
    private String refreshTokenExpiredDate;

}
