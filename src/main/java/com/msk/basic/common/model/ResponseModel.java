package com.msk.basic.common.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class ResponseModel<T> {

    @ApiModelProperty(value = "결과 코드", example = "SUC001", position = 1)
    private String code;

    @ApiModelProperty(value = "결과 메시지", example = "처리가 완료되었습니다.", position = 2)
    private String message;

    @ApiModelProperty(value = "결과 데이터", position = 3)
    private Object data;

}
