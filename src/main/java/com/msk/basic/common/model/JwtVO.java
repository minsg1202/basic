package com.msk.basic.common.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class JwtVO {
    int userId;
}
