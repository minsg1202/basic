package com.msk.basic.common.model;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import springfox.documentation.annotations.ApiIgnore;

@Data
public class BaseRequest {
    @ApiParam(hidden = true)
    @ApiModelProperty(hidden = true)
    public Long custSno;
}
