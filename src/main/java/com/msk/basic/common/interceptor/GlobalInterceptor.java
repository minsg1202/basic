package com.msk.basic.common.interceptor;

import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.msk.basic.common.config.SwaggerConfig;
import com.msk.basic.common.enumeration.ErrorCode;
import com.msk.basic.common.exception.CustomException;
import com.msk.basic.common.util.JwtUtil;
import lombok.AllArgsConstructor;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class GlobalInterceptor extends HandlerInterceptorAdapter {

    private JwtUtil jwtUtil;

    @Override
    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler) throws Exception {
        log.debug("============ GlobalInterceptor Start ===============");

        String token = request.getHeader(SwaggerConfig.HEADER_AUTH);
        log.debug("[INFO] [ACTIONLOG] [TOKEN] accessToken = {}", token);
        if (!StringUtils.isEmpty(token)) {
            // Bearer 체크
            if (Pattern.matches("^Bearer .*", token)) {
                token = token.replaceAll("^Bearer( )*", "");
                Long custSno = jwtUtil.isUseableToken(token);

                request.setAttribute("custSno", custSno);
                log.debug("[INFO] [ACTIONLOG] [TOKEN] Login custSno = {}", custSno);
            } else {
                throw new CustomException(ErrorCode.TOKEN_FALSIFY);
            }
        }
        log.debug("============ GlobalInterceptor End ===============");

        return super.preHandle(request, response, handler);
    }

}
