package com.msk.basic.common.advice;

import com.msk.basic.common.enumeration.ErrorCode;
import com.msk.basic.common.exception.CustomException;
import com.msk.basic.common.util.RestUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice(annotations = RestController.class)
public class AdviceController {

    @ExceptionHandler(value = {CustomException.class})
    protected ResponseEntity<?> customException(final CustomException e) {
        log.error("customException", e);

        ErrorCode errorCode = e.getErrorCode();

        if (errorCode.equals(ErrorCode.TOKEN_EXPIRED ) || errorCode.equals(ErrorCode.TOKEN_FALSIFY)) {
            return RestUtil.tokenError(errorCode);
        }

        return RestUtil.error(errorCode);
    }

    @ExceptionHandler(value = {BindException.class})
    public ResponseEntity<?> bindException(final BindException e , final BindingResult bindingResult) {
        log.error("bindException (validationException)", e);

//        ErrorField errorField = ErrorField.of(bindingResult);

        return RestUtil.error();
    }

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<?> serverException(final Exception e) {
        log.error("serverException", e);

        return RestUtil.serverError(e.getMessage());
    }

}
